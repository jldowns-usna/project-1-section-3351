# This is a simple networked application that can both send and
# receive data. Think of it as "telnet".
from threading import Thread

import sys
import os
sys.path.append('../')
import layer4.stub_layer_4 as Layer4
import logging
logging.basicConfig(level=logging.DEBUG)
import subprocess

class SimpleApp:
    def __init__(self, layer4, port, name="Unnamed"):
        """Create a client "process".

        We pass a layer 5 object so that we don't instantiate
        more than one "network stack."
        """

        # Save the layer 4 object as a instance variable
        # so we can reference it later
        self.layer4 = layer4

        self.name = name

        # Open a new socket to listen on. Here we're choosing
        # port 80 (just as an example.)
        logging.debug(f"{name} connecting on port {port}")
        self.layer4.connect_to_socket(port, self.receive)

        # help statement
        print("Usage message:")
        print()
        print("Enter destination and message with each entry.")
        print()
        print("Destination information:")
        print("President - 1")
        print("Ops - 2")
        print("Weapon - 3")
        print()
        print("Weapon Commands:")
        print("fire - fires weapon")
           
    def receive(self, data):
        # Do we need to handle any packet formating/de-encapusulation? No
        """Receive and handle a message.
        """

        print(f"{self.name} received message: {data}")

        if(data == "fire"):
            servo()


    def send(self, receiver_addr, data):
        """Sends a message to a receiver.
        """

        # Send the message to layer 4. We haven't implemented
        # proper addresses yet so we're setting the port numbers
        # and addr to None.
        # get destination address from user 

        self.layer4.from_layer_5(data=data, src_port=None,
                dest_port=90, dest_addr=receiver_addr) 

# main

layer4 = Layer4.StubLayer4()

def start_server():
    server = SimpleApp(layer4, 90, "Server")
    print("Server online.")
    while True:
        pass

def start_client():
    # Authenticate user 
    status = False
    username = input("Enter Username: ")
    password = input("Enter Password: ")
    if not check(username, password):
        print("Error. Either password or username are incorrect")
        start_client()
    else:
        # Start client message loop.
        client = SimpleApp(layer4, 91, username)
        while True:
            address = destination(input("Dest Address: "))
            msg = input("Msg: ")

            if address == 3:
                if msg == "fire":
                    servo()
                    client.send(address,msg)
                else:
                    print("Invalid command")
                    print()
                    print("Weapon Commands:")
                    print("fire - fires weapon")
            else:
                # We haven't implemented an address scheme, so we will just
                # pass the receiver_addr = None.
                client.send(address, msg)

def check(user,pword):
    if user=="pres" and pword=="2024":
        return True
    elif user=="ops" and pword=="vip":
        return True
    elif user=="wep" and pword=="shoot":
        return True
    else:
        return False

def destination(address):
    # need port numbers from Rachel
    if(address=="1"):
        return 92
    elif(address=="2"):
        return 93
    elif(address=="3"):
        return 94
    else:
        print("Entered invalid option.")
        print()
        print("Destination information:")
        print("President - 1")
        print("Ops - 2")
        print("Weapon - 3")

def servo():
    subprocess.run(["python3", "servo.py"])

t = Thread(target=start_server, args = ())
t.start()
t = Thread(target=start_client, args = ())
t.start()

# send layer 4:
# dest. port / IP address (bin)
# source port 

# need to integrate weapons controller 
# need port information from Rachel (pres, ops, weapon)
