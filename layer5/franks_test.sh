# This runs the files.

python3 run-applications.py \
	--input1=13 --output1=11 \
	--input2=31 --output2=29 \
	--input3=18 --output3=16 \
	--clock_interval1=$((10**8)) \
	--clock_interval2=$((10**8)) \
	--clock_interval3=$((10**8))
